import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}
int leftCardnamber=2;
int rightCardnamber=4;
 int plyarResult=0;
 int cpuResult=0;
 int totalResul=0;

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SafeArea(
        child: Scaffold(
          //backgroundColor: Colors.green,
          body: Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/background.jpg'))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  child: Image.asset(
                    'assets/images/logo.jpeg',
                    height: 200,
                    width: 200,
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    const SizedBox(
                      height: 15,
                    ),
                    Image.asset('assets/images/card$leftCardnamber.png'),
                    const SizedBox(
                      width: 30,
                    ),
                    Image.asset('assets/images/card$rightCardnamber.png')
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                InkWell(
                     
                    onTap: () {
                      int newleftCardnamber=Random().nextInt(13);
                      int newrightCardnamber=Random().nextInt(13);


                      setState(() {
                        if (newleftCardnamber==0||newleftCardnamber==1){
                          leftCardnamber=newleftCardnamber+2;
                        }else{
                          leftCardnamber=newleftCardnamber;
                        }

                        if(newrightCardnamber==0||newrightCardnamber==1){
                          rightCardnamber=newrightCardnamber+2;
                        }else{
                          rightCardnamber=newrightCardnamber;
                        }
                        if( leftCardnamber>rightCardnamber){

                          plyarResult++;
                          //plyarResult=plyarResult+1;
                        }else{
                          cpuResult++;

                          totalResul=plyarResult+cpuResult;
                        }
                        if(totalResul>9){

                          cpuResult=0;
                          plyarResult=0;
                        }else{
                          totalResul=0 ;
                        }


                       // leftCardnamber=3;
                       // rightCardnamber=6;
                      });

                    },
                    child: Image.asset('assets/images/dealbutton.png'),
                  ),

                SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children:   [
                        Text(
                          'Plyer',
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          "$plyarResult",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                    Column(
                      children:   [
                        Text(
                          'CPU',
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          '$cpuResult',
                          style: TextStyle(color: Colors.white),
                        ),
                      ],
                    ),
                    Text('total Playe game:${plyarResult + cpuResult}')
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
